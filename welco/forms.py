# welco - multichannel request processing
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.utils.translation import gettext_lazy as _

from .utils import get_wcs_options


class QualificationForm(forms.Form):
    formdef_reference = forms.CharField(label=_('Associated Form'))

    def __init__(self, user, *args, **kwargs):
        super().__init__(*args, **kwargs)
        params = {'backoffice-submission': 'on'}
        if hasattr(user, 'saml_identifiers') and user.saml_identifiers.exists():
            params['NameID'] = user.saml_identifiers.first().name_id
        formdef_references = get_wcs_options('api/formdefs/', params=params)
        self.fields['formdef_reference'].widget = forms.Select(choices=formdef_references)
