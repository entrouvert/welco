# welco - multichannel request processing
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.utils.translation import gettext_lazy as _
from django.utils.translation import pgettext_lazy

DEFAULT_TITLE_CHOICES = (
    ('', ''),
    (pgettext_lazy('title', 'Mrs'), pgettext_lazy('title', 'Mrs')),
    (pgettext_lazy('title', 'Mr'), pgettext_lazy('title', 'Mr')),
)


class ContactAddForm(forms.Form):
    title = forms.CharField(
        label=_('Title'), required=False, widget=forms.Select(choices=DEFAULT_TITLE_CHOICES)
    )
    first_name = forms.CharField(label=_('First Name'), required=False)
    last_name = forms.CharField(
        label=_('Last Name'), required=True, widget=forms.TextInput(attrs={'required': 'required'})
    )
    email = forms.CharField(label=_('Email'), required=False)
    address = forms.CharField(label=_('Address'), required=False)
    zipcode = forms.CharField(label=_('Zip Code'), required=False)
    city = forms.CharField(label=_('City'), required=False)
    country = forms.CharField(label=_('Country'), required=False)
    phone = forms.CharField(label=_('Phone'), required=False)
    mobile = forms.CharField(label=_('Mobile'), required=False)
    birthdate = forms.CharField(label=_('Birthdate'), required=False)
