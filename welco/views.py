# welco - multichannel request processing
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from django import template
from django.conf import settings
from django.contrib.auth import logout as auth_logout
from django.contrib.auth import views as auth_views
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied
from django.http import HttpResponse, HttpResponseBadRequest, HttpResponseRedirect
from django.shortcuts import resolve_url
from django.urls import reverse
from django.utils.encoding import force_str
from django.utils.http import quote
from django.utils.translation import gettext_lazy as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from .contacts.views import HomeZone as ContactsHomeZone
from .forms import QualificationForm
from .kb.views import HomeZone as KbHomeZone
from .kb.views import check_user_perms as check_kb_user_perms
from .qualif.models import Association
from .sources.counter.views import Home as CounterHome
from .sources.mail.views import Home as MailHome
from .sources.phone.views import Home as PhoneHome

if 'mellon' in settings.INSTALLED_APPS:
    from mellon.utils import get_idps
else:

    def get_idps():
        return []


class LoginView(auth_views.LoginView):
    template_name = 'welco/login.html'

    def dispatch(self, request, *args, **kwargs):
        if any(get_idps()):
            if 'next' not in request.GET:
                return HttpResponseRedirect(resolve_url('mellon_login'))
            try:
                quoted_next_url = quote(request.GET.get('next'))
            except KeyError:
                return HttpResponseBadRequest('invalid value for "next" parameter')
            return HttpResponseRedirect(resolve_url('mellon_login') + '?next=' + quoted_next_url)
        return super().dispatch(request, *args, **kwargs)


login = LoginView.as_view()


def logout(request, next_page=None):
    if any(get_idps()):
        return HttpResponseRedirect(resolve_url('mellon_logout'))
    auth_logout(request)
    if next_page is not None:
        next_page = resolve_url(next_page)
    else:
        next_page = '/'
    return HttpResponseRedirect(next_page)


class Qualification(TemplateView):
    template_name = 'welco/qualification_no_validation.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = QualificationForm(self.request.user)
        context['source_type'] = self.request.GET['source_type']
        source_type = ContentType.objects.get(id=self.request.GET['source_type'])
        context['source_type_name'] = source_type.model
        context['source_pk'] = self.request.GET['source_pk']
        if self.request.GET.get('source_pk'):
            context['associations'] = Association.objects.filter(
                source_type=ContentType.objects.get(id=self.request.GET['source_type']),
                source_pk=self.request.GET['source_pk'],
            ).order_by('id')
        return context

    def post(self, request, *args, **kwargs):
        association = Association(
            source_type=ContentType.objects.get(id=request.POST['source_type']),
            source_pk=request.POST['source_pk'],
        )
        association.formdef_reference = request.POST['formdef_reference']
        association.save()
        request.GET = request.POST
        return self.get(request)


qualification = csrf_exempt(Qualification.as_view())


class ChannelHome(TemplateView):
    template_name = 'welco/home.html'
    source_klass = MailHome

    def check_user_ok(self):
        user_groups = {x.name for x in self.request.user.groups.all()}
        channel_groups = set(settings.CHANNEL_ROLES[self.source_klass.source_key])
        return user_groups.intersection(channel_groups)

    def get_context_data(self, **kwargs):
        if not self.check_user_ok():
            raise PermissionDenied()
        context = super().get_context_data(**kwargs)
        context['panels'] = [{'key': x, 'zone_url': x + '-zone'} for x in settings.SCREEN_PANELS]
        context['source'] = self.source_klass(self.request, **kwargs)
        context['kb'] = KbHomeZone(self.request)
        context['contacts'] = ContactsHomeZone(self.request)
        context['channels'] = []

        user_groups = {x.name for x in self.request.user.groups.all()}
        for channel in settings.CHANNEL_ROLES:
            channel_groups = set(settings.CHANNEL_ROLES[channel])
            if user_groups.intersection(channel_groups):
                context['channels'].append(channel)
        return context


@login_required
def home(request):
    user_groups = {x.name for x in request.user.groups.all()}
    for channel in settings.CHANNEL_ROLES:
        channel_groups = set(settings.CHANNEL_ROLES[channel])
        if user_groups.intersection(channel_groups):
            return HttpResponseRedirect('%s/' % channel)
    raise PermissionDenied()


class HomePhone(ChannelHome):
    source_klass = PhoneHome


home_phone = login_required(HomePhone.as_view())


class HomeMail(ChannelHome):
    source_klass = MailHome


home_mail = login_required(HomeMail.as_view())


class HomeCounter(ChannelHome):
    source_klass = CounterHome


home_counter = login_required(HomeCounter.as_view())


@login_required
def wcs_summary(request, *args, **kwargs):
    source_class = ContentType.objects.get(id=kwargs.get('source_type')).model_class()
    source_object = source_class.objects.get(id=kwargs.get('source_pk'))
    tmpl_name = 'welco/%s_summary.html' % source_class._meta.app_label
    tmpl = template.loader.get_template(tmpl_name)
    context = {}
    context['site_base'] = request.build_absolute_uri('/')[:-1]
    context['object'] = source_object
    json_str = json.dumps({'content': tmpl.render(context, request=request)})
    for variable in ('jsonpCallback', 'callback'):
        if variable in request.GET:
            json_str = '%s(%s);' % (request.GET[variable], json_str)
            break
    return HttpResponse(json_str, content_type='application/javascript')


@login_required
def remove_association(request, *args, **kwargs):
    Association.objects.filter(id=kwargs.get('pk')).delete()
    return HttpResponseRedirect(resolve_url('home'))


@login_required
@csrf_exempt
def create_formdata(request, *args, **kwargs):
    response = HttpResponse(content_type='application/json')
    if request.method != 'POST':
        json.dump({'err': 1}, response)
        return response

    qualif = Association.objects.get(id=kwargs.get('pk'))
    try:
        qualif.push(request)
    except Exception as e:
        json.dump({'err': 1, 'msg': str(e)}, response)
        return response
    json.dump({'result': 'ok', 'url': qualif.formdata_url}, response)
    return response


@login_required
def menu_json(request):
    response = HttpResponse(content_type='application/json')
    user_groups = {x.name for x in request.user.groups.all()}
    menu = []
    labels = {
        'mail': _('Mails'),
        'phone': _('Call Center'),
        'counter': _('Counter'),
    }
    for channel in settings.CHANNEL_ROLES:
        channel_groups = set(settings.CHANNEL_ROLES[channel])
        if user_groups.intersection(channel_groups):
            menu.append(
                {
                    'label': force_str(labels.get(channel)),
                    'slug': channel,
                    'url': request.build_absolute_uri(reverse('home-%s' % channel)),
                }
            )
    if check_kb_user_perms(request.user, access=True):
        menu.append(
            {
                'label': force_str(_('Knowledge Base')),
                'slug': 'book',
                'url': request.build_absolute_uri(reverse('kb-home')),
            }
        )
    json_str = json.dumps(menu)
    for variable in ('jsonpCallback', 'callback'):
        if variable in request.GET:
            response = HttpResponse(content_type='application/javascript')
            json_str = '%s(%s);' % (request.GET[variable], json_str)
            break
    response.write(json_str)
    return response
