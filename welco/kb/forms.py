# welco - multichannel request processing
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.utils.text import slugify

from .models import Page


class PageForm(forms.ModelForm):
    class Meta:
        model = Page
        exclude = ('slug',)

    def save(self, commit=True):
        if not self.instance.slug:
            base_slug = slugify(self.instance.title)[:40]
            slug = base_slug
            i = 1
            while True:
                try:
                    self.Meta.model.objects.get(slug=slug)
                except self.Meta.model.DoesNotExist:
                    break
                i += 1
                slug = '%s-%s' % (base_slug, i)
            self.instance.slug = slug
        return super().save(commit=commit)
