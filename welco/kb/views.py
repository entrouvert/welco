# welco - multichannel request processing
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from django import template
from django.conf import settings
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.exceptions import PermissionDenied
from django.db.models import Count
from django.http import HttpResponse
from django.urls import reverse_lazy
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import CreateView, DeleteView, DetailView, ListView, TemplateView, UpdateView
from haystack.forms import SearchForm
from haystack.generic_views import SearchView
from haystack.query import SearchQuerySet
from taggit.models import Tag

from .forms import PageForm
from .models import Page


def check_user_perms(user, access=False):
    allowed_roles = settings.KB_MANAGE_ROLES[:]
    if access:
        allowed_roles.extend(settings.KB_ACCESS_ROLES)
    if settings.KB_ROLE:
        allowed_roles.append(settings.KB_ROLE)  # legacy
    user_groups = {x.name for x in user.groups.all()}
    return user_groups.intersection(allowed_roles)


def check_request_perms(request, access=False):
    if not check_user_perms(request.user, access=access):
        raise PermissionDenied()


class PageListView(ListView):
    model = Page

    def dispatch(self, request, *args, **kwargs):
        check_request_perms(request, access=True)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['form'] = SearchForm()
        context['can_manage'] = check_user_perms(self.request.user)
        return context


page_list = login_required(PageListView.as_view())


class PageAddView(CreateView):
    model = Page
    form_class = PageForm

    def dispatch(self, request, *args, **kwargs):
        check_request_perms(request)
        return super().dispatch(request, *args, **kwargs)


page_add = login_required(PageAddView.as_view())


class PageEditView(UpdateView):
    model = Page
    form_class = PageForm

    def dispatch(self, request, *args, **kwargs):
        check_request_perms(request)
        return super().dispatch(request, *args, **kwargs)


page_edit = login_required(PageEditView.as_view())


class PageDetailView(DetailView):
    model = Page

    def dispatch(self, request, *args, **kwargs):
        check_request_perms(request, access=True)
        return super().dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['can_manage'] = check_user_perms(self.request.user)
        return context


page_detail = login_required(PageDetailView.as_view())


class PageDetailFragmentView(DetailView):
    model = Page
    template_name = 'kb/page_detail_fragment.html'


page_detail_fragment = PageDetailFragmentView.as_view()


class PageDeleteView(DeleteView):
    model = Page
    success_url = reverse_lazy('kb-home')

    def dispatch(self, request, *args, **kwargs):
        check_request_perms(request)
        return super().dispatch(request, *args, **kwargs)


page_delete = login_required(PageDeleteView.as_view())


class PageSearchView(SearchView):
    template_name = 'kb/page_search.html'
    form_class = SearchForm

    def dispatch(self, request, *args, **kwargs):
        check_request_perms(request, access=True)
        return super().dispatch(request, *args, **kwargs)


page_search = login_required(PageSearchView.as_view())


class KbZone(TemplateView):
    template_name = 'kb/zone.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['source_pk'] = self.request.GET.get('source_pk')
        context['form'] = SearchForm()
        context['tags'] = (
            Tag.objects.all().annotate(num_times=Count('taggit_taggeditem_items')).filter(num_times__gt=0)
        )
        num_times = context['tags'].values_list('num_times', flat=True)
        if not num_times:
            num_times = [0]
        delta = max(num_times) - min(num_times)
        for tag in context['tags']:
            if delta == 0:
                tag.font_size = 'normal'
            else:
                factor = 1.0 * (tag.num_times - min(num_times)) / delta
                if factor < 0.2:
                    tag.font_size = 'x-small'
                elif factor < 0.4:
                    tag.font_size = 'small'
                elif factor < 0.6:
                    tag.font_size = 'normal'
                elif factor < 0.8:
                    tag.font_size = 'large'
                else:
                    tag.font_size = 'x-large'
        return context


zone = csrf_exempt(KbZone.as_view())


def page_search_json(request):
    query = request.GET.get('q')
    searchqueryset = SearchQuerySet()
    sqs = searchqueryset.autocomplete(text_auto=query)
    sqs.load_all()

    result = []
    for item in sqs:
        result.append({'title': item.title, 'slug': item.slug})

    response = HttpResponse(content_type='application/json')
    json.dump({'data': result}, response, indent=2)
    return response


class HomeZone:
    def __init__(self, request):
        self.request = request

    def render(self):
        tmpl = template.loader.get_template('kb/zone.html')
        return tmpl.render(request=self.request)


def kb_manager_required(view_func):
    return user_passes_test(check_user_perms)(view_func)
