from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('kb', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='keywords',
            field=models.CharField(max_length=200, verbose_name='Keywords', blank=True),
            preserve_default=True,
        ),
    ]
