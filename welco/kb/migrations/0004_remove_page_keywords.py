from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('kb', '0003_page_tags'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='page',
            name='keywords',
        ),
    ]
