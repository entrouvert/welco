import taggit.managers
from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('taggit', '0002_auto_20150616_2121'),
        ('kb', '0002_page_keywords'),
    ]

    operations = [
        migrations.AddField(
            model_name='page',
            name='tags',
            field=taggit.managers.TaggableManager(
                to='taggit.Tag',
                through='taggit.TaggedItem',
                blank=True,
                help_text='A comma-separated list of tags.',
                verbose_name='Keywords',
            ),
            preserve_default=True,
        ),
    ]
