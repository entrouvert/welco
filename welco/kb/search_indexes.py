# welco - multichannel request processing
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import html

from django.utils.html import strip_tags
from haystack import indexes

from .models import Page


class PageIndex(indexes.SearchIndex, indexes.Indexable):
    title = indexes.CharField(model_attr='title', boost=3)
    text = indexes.CharField(document=True)
    text_auto = indexes.EdgeNgramField()
    slug = indexes.CharField(model_attr='slug', indexed=False)
    tags = indexes.CharField(boost=1.5)

    def get_model(self):
        return Page

    def prepare_text(self, obj):
        return obj.title + ' ' + self.prepare_tags(obj) + ' ' + html.unescape(strip_tags(obj.content))

    def prepare_text_auto(self, obj):
        return self.prepare_text(obj)

    def prepare_tags(self, obj):
        return ' '.join([tag.name for tag in obj.tags.all()])
