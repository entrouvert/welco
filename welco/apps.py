# welco - multichannel request processing
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.apps import apps
from django.urls import include, path


def register_urls(urlpatterns):
    pre_urls = []
    post_urls = []
    for app in apps.get_app_configs():
        if hasattr(app, 'get_before_urls'):
            urls = app.get_before_urls()
            if urls:
                pre_urls.append(path('', include(urls)))
        if hasattr(app, 'get_after_urls'):
            urls = app.get_after_urls()
            if urls:
                post_urls.append(path('', include(urls)))
    return pre_urls + urlpatterns + post_urls
