# welco - multichannel request processing
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

from django import template
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import TemplateView

from .models import CounterPresence


class Home:
    source_key = 'counter'

    def __init__(self, request, **kwargs):
        self.request = request
        self.kwargs = kwargs

    def render(self):
        zone = CounterZone()
        zone.request = self.request
        tmpl = template.loader.get_template('welco/counter_home.html')
        return tmpl.render(zone.get_context_data(), request=self.request)


class CounterZone(TemplateView):
    template_name = 'welco/counter_home.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['source_type'] = ContentType.objects.get_for_model(CounterPresence)
        new_source = CounterPresence()
        new_source.save()
        context['source_pk'] = new_source.id
        context['useful_links'] = settings.COUNTER_LINKS
        return context


zone = csrf_exempt(CounterZone.as_view())


@csrf_exempt
@login_required
def next(request):
    return HttpResponse(json.dumps({'err': 0}), content_type='application/json')
