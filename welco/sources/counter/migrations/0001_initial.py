from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = []

    operations = [
        migrations.CreateModel(
            name='CounterPresence',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('status', models.CharField(max_length=50, verbose_name='Status', blank=True)),
                ('contact_id', models.CharField(max_length=50, null=True)),
                ('creation_timestamp', models.DateTimeField(auto_now_add=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True)),
            ],
            options={
                'verbose_name': 'Counter Presence',
            },
            bases=(models.Model,),
        ),
    ]
