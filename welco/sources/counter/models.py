# welco - multichannel request processing
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.utils.translation import gettext_lazy as _

from welco.qualif.models import Association


class CounterPresence(models.Model):
    class Meta:
        verbose_name = _('Counter Presence')

    # common to all source types:
    status = models.CharField(_('Status'), blank=True, max_length=50)
    contact_id = models.CharField(max_length=50, null=True)
    associations = GenericRelation(Association, content_type_field='source_type', object_id_field='source_pk')

    creation_timestamp = models.DateTimeField(auto_now_add=True)
    last_update_timestamp = models.DateTimeField(auto_now=True)

    @classmethod
    def get_qualification_form_class(cls):
        return None

    def get_qualification_form(self):
        return None

    def get_source_context(self, request):
        return {
            'channel': 'counter',
        }
