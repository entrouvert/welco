# welco - multichannel request processing
# Copyright (C) 2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import re
import subprocess

import requests
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from welco.qualif.models import Association
from welco.utils import get_wcs_data


class Mail(models.Model):
    class Meta:
        verbose_name = _('Mail')
        ordering = ['post_date', 'creation_timestamp']

    content = models.FileField(_('Content'))
    post_date = models.DateField(_('Post Date'), null=True)
    registered_mail_number = models.CharField(_('Registered Mail Number'), null=True, max_length=50)
    note = models.TextField(_('Note'), null=True)
    external_id = models.CharField(_('External Id'), null=True, max_length=32)

    reference = models.CharField(_('Reference'), null=True, max_length=30)
    subject = models.CharField(_('Subject'), null=True, max_length=200)

    scanner_category = models.CharField(max_length=100, blank=True, null=True)

    # common to all source types:
    status = models.CharField(_('Status'), blank=True, max_length=50)
    contact_id = models.CharField(max_length=50, null=True)
    associations = GenericRelation(Association, content_type_field='source_type', object_id_field='source_pk')

    creation_timestamp = models.DateTimeField(auto_now_add=True)
    last_update_timestamp = models.DateTimeField(auto_now=True)

    @classmethod
    def get_qualification_form_class(cls):
        from .forms import MailQualificationForm

        return MailQualificationForm

    def get_qualification_form(self):
        data = {
            'post_date': self.post_date,
            'registered_mail_number': self.registered_mail_number,
        }
        return self.get_qualification_form_class()(data)

    @classmethod
    def get_qualification_form_submit_url(cls):
        return reverse('qualif-mail-save')

    def contact_name(self):
        if not self.contact_id:
            return ''
        try:
            user_details = get_wcs_data('api/users/%s/' % self.contact_id)
        except requests.HTTPError:
            return _('unknown')
        return user_details.get('user_display_name')

    def html_note(self):
        return re.sub(r'[\r?\n]+', '<br><br>', self.note, re.DOTALL)

    def categories(self):
        categories = {}
        for association in self.associations.all():
            if association.formdef_category:
                categories[association.formdef_category] = True
        if not categories and self.scanner_category:
            categories[self.scanner_category] = True
        if not categories:
            return '-'
        return categories.keys()

    def get_source_context(self, request):
        context = {
            'channel': 'mail',
            'post_date': self.post_date and self.post_date.strftime('%Y-%m-%d'),
            'registered_mail_number': self.registered_mail_number,
            'external_id': self.external_id,
        }
        return context


@receiver(post_save, sender=Mail)
def create_thumbnail(sender, instance, created, **kwargs):
    if not created:
        return
    subprocess.call(
        [
            'gm',
            'convert',
            '-geometry',
            '200x',
            instance.content.file.name,
            instance.content.file.name + '.png',
        ]
    )
