# welco - multichannel request processing
# Copyright (C) 2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import django.apps


class AppConfig(django.apps.AppConfig):
    name = 'welco.sources.mail'

    def get_before_urls(self):
        from . import urls

        return urls.urlpatterns

    def ready(self):
        from django.db.models import signals

        from welco.qualif.models import Association

        signals.post_save.connect(self.association_post_save, sender=Association)

    def association_post_save(self, sender, instance, **kwargs):
        from .utils import get_maarch

        if not instance.formdata_id:
            return
        source = instance.source
        if not getattr(source, 'external_id', None):
            return
        external_id = source.external_id
        if not external_id.startswith('maarch-'):
            return
        maarch_pk = int(external_id.split('-', 1)[-1])
        maarch = get_maarch()
        maarch.set_grc_sent_status(
            mail_pk=maarch_pk,
            formdata_id=instance.formdata_id,
            formdata_url_backoffice=instance.formdata_url_backoffice,
        )
