# welco - multichannel request processing
# Copyright (C) 2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.conf import settings

from .maarch import MaarchCourrier


class WelcoMaarchCourrier(MaarchCourrier):
    def __init__(
        self,
        url,
        username,
        password,
        grc_status,
        grc_received_status,
        grc_send_status,
        grc_refused_status,
        grc_response_status,
        batch_size=10,
    ):
        super().__init__(url, username, password)
        self.grc_status = grc_status
        self.grc_received_status = grc_received_status
        self.grc_send_status = grc_send_status
        self.grc_refused_status = grc_refused_status
        self.grc_response_status = grc_response_status
        self.batch_size = batch_size

    def get_mails(self):
        return self.get_courriers(
            clause="status='%s'" % self.grc_status,
            include_file=True,
            order_by=['res_id'],
            limit=self.batch_size,
        )

    def get_mail(self, mail_id):
        return self.get_courriers(clause='res_id=%s' % mail_id)[0]

    def set_grc_received_status(self, mails):
        self.update_status(mails, self.grc_received_status)

    def set_grc_sent_status(self, mail_pk, formdata_id, formdata_url_backoffice):
        mail = self.Courrier(self, pk=mail_pk)
        mail.external_id = str(formdata_id)
        mail.external_link = formdata_url_backoffice
        self.update_external_infos([mail], self.grc_send_status)

    def set_grc_refused_status(self, mail_pk):
        mail = self.Courrier(self, pk=mail_pk)
        self.update_status([mail], self.grc_refused_status)

    def set_grc_response_status(self, mail_pk, history_message):
        mail = self.Courrier(self, pk=mail_pk)
        self.update_status([mail], self.grc_response_status, history_message)


def get_maarch():
    config = getattr(settings, 'MAARCH_FEED', {})
    if not config.get('ENABLE'):
        return
    url = config['URL']
    username = config['USERNAME']
    password = config['PASSWORD']
    return WelcoMaarchCourrier(
        url=url,
        username=username,
        password=password,
        grc_status=config.get('STATUS_GRC', 'GRC'),
        grc_received_status=config.get('STATUS_RECEIVED', 'GRC_TRT'),
        grc_send_status=config.get('STATUS_SEND', 'GRCSENT'),
        grc_refused_status=config.get('STATUS_REFUSED', 'GRCREFUSED'),
        grc_response_status=config.get('STATUS_RESPONSE', 'GRC_RESPONSE'),
    )
