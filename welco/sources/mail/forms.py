# welco - multichannel request processing
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django import forms
from django.utils.translation import gettext_lazy as _


class MailQualificationForm(forms.Form):
    post_date = forms.DateTimeField(label=_('Post Date (*)'), required=False)
    registered_mail_number = forms.CharField(label=_('Registered Mail Number'), required=False)
    reference = forms.CharField(label=_('Reference'), required=False, widget=forms.HiddenInput)
    subject = forms.CharField(label=_('Subject'), required=False, widget=forms.HiddenInput)
