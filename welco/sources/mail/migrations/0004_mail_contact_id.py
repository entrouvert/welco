from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mail', '0003_auto_20151009_1144'),
    ]

    operations = [
        migrations.AddField(
            model_name='mail',
            name='contact_id',
            field=models.CharField(max_length=50, null=True),
            preserve_default=True,
        ),
    ]
