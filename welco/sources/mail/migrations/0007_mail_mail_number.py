from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mail', '0006_mail_note'),
    ]

    operations = [
        migrations.AddField(
            model_name='mail',
            name='mail_number',
            field=models.CharField(max_length=50, null=True, verbose_name='Mail Number'),
            preserve_default=True,
        ),
    ]
