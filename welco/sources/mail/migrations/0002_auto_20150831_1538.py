from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mail', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='mail',
            name='post_date',
            field=models.DateField(null=True, verbose_name='Post Date'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='mail',
            name='registered_mail',
            field=models.BooleanField(default=False, verbose_name='Registered Mail'),
            preserve_default=True,
        ),
    ]
