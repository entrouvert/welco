from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('mail', '0007_mail_mail_number'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mail',
            name='mail_number',
        ),
    ]
