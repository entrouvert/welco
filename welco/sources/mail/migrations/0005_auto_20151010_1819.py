from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mail', '0004_mail_contact_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mail',
            name='registered_mail',
        ),
        migrations.AddField(
            model_name='mail',
            name='registered_mail_number',
            field=models.CharField(max_length=50, null=True, verbose_name='Registered Mail Number'),
            preserve_default=True,
        ),
    ]
