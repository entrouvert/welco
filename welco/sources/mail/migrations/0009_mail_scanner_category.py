from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mail', '0008_remove_mail_mail_number'),
    ]

    operations = [
        migrations.AddField(
            model_name='mail',
            name='scanner_category',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
    ]
