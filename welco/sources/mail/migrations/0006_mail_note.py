from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mail', '0005_auto_20151010_1819'),
    ]

    operations = [
        migrations.AddField(
            model_name='mail',
            name='note',
            field=models.TextField(null=True, verbose_name='Note'),
            preserve_default=True,
        ),
    ]
