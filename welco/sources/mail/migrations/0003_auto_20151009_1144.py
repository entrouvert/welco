from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mail', '0002_auto_20150831_1538'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='mail',
            name='triaged',
        ),
        migrations.AddField(
            model_name='mail',
            name='status',
            field=models.CharField(max_length=50, verbose_name='Status', blank=True),
            preserve_default=True,
        ),
    ]
