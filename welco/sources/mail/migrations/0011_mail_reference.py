from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mail', '0010_mail_subject'),
    ]

    operations = [
        migrations.AddField(
            model_name='mail',
            name='reference',
            field=models.CharField(max_length=30, null=True, verbose_name='Reference'),
        ),
        migrations.AlterModelOptions(
            name='mail',
            options={'ordering': ['post_date', 'creation_timestamp'], 'verbose_name': 'Mail'},
        ),
    ]
