from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mail', '0009_mail_scanner_category'),
    ]

    operations = [
        migrations.AddField(
            model_name='mail',
            name='subject',
            field=models.CharField(max_length=200, null=True, verbose_name='Subject'),
        ),
    ]
