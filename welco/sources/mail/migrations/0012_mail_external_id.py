from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('mail', '0011_mail_reference'),
    ]

    operations = [
        migrations.AddField(
            model_name='mail',
            name='external_id',
            field=models.CharField(max_length=32, null=True, verbose_name='External Id'),
        ),
    ]
