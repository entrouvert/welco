window.addEventListener('message', function(event) {
  if (typeof(PDFViewerApplication) !== 'undefined') {
    PDFViewerApplication.open(event.data);
  } else {
    PDFView.open(event.data);
  }
}, false);
