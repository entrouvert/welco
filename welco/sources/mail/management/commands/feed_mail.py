# welco - multichannel request processing
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os

from django.core.files.base import ContentFile
from django.core.management.base import BaseCommand, CommandError

from ...models import Mail


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--category', metavar='CATEGORY', default=None)
        parser.add_argument('filenames', metavar='FILENAME', nargs='+')

    def handle(self, filenames, *args, **kwargs):
        count = 0
        for filepath in filenames:
            if not os.path.exists(filepath):
                continue
            with open(filepath) as fp:
                if not fp.read(5) == '%PDF-':
                    continue
            with open(filepath) as fp:
                mail = Mail(content=ContentFile(fp.read(), name=os.path.basename(filepath)))
            mail.scanner_category = kwargs.get('category')
            mail.save()
            count += 1
        if count == 0:
            raise CommandError('nothing got imported')
