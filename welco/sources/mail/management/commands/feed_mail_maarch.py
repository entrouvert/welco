# welco - multichannel request processing
# Copyright (C) 2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.core.files.base import ContentFile
from django.core.management.base import BaseCommand
from django.db import transaction

from ...models import Mail
from ...utils import get_maarch


class Command(BaseCommand):
    """Inject mail coming from Maarch into welco.

    Only mail with a status "GRC" are injected,
    After injection, their status is immediately changed to "GRC_TRT".
    After injection in w.c.s., their status is changed to "GRCSENT" and an
    id and an URL of the request in w.c.s. is attached to the mail in
    Maarch.
    """

    def handle(self, *args, **kwargs):
        verbosity = kwargs['verbosity']
        maarch = get_maarch()
        if not maarch:
            if verbosity > 1:
                self.stdout.write('Maarch is not configured.')
            return

        maarch_mails = maarch.get_mails()
        count = 0
        while maarch_mails:
            with transaction.atomic():
                for maarch_mail in maarch_mails:
                    Mail.objects.create(
                        content=ContentFile(maarch_mail.content, name='maarch-%s' % maarch_mail.pk),
                        external_id='maarch-%s' % str(maarch_mail.pk),  # res_id
                    )
                # update maarch inside transaction, if it fails all imports will be
                # rollbacked
                maarch.set_grc_received_status(maarch_mails)
                count += len(maarch_mails)
            maarch_mails = maarch.get_mails()
        if verbosity > 1:
            self.stdout.write('Injected %d mails from %s.' % (count, maarch.url))
