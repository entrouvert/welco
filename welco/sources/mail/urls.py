# welco - multichannel request processing
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import path, re_path

from .views import edit_note, feeder, mail_count, mail_response, note, qualification_save, reject, viewer

urlpatterns = [
    path('mail/viewer/', viewer, name='mail-viewer'),
    path('mail/feeder/', feeder, name='mail-feeder'),
    path('ajax/mail/reject', reject, name='mail-reject'),
    path('ajax/qualification-mail-save', qualification_save, name='qualif-mail-save'),
    path('ajax/mail/edit-note/', edit_note, name='mail-edit-note'),
    re_path(r'^ajax/mail/note/(?P<pk>\w+)$', note, name='mail-note'),
    path('ajax/count/mail/', mail_count, name='mail-count'),
    path('api/mail/response/', mail_response, name='mail-api-response'),
]
