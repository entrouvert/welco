# welco - multichannel request processing
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import logging

from django.conf import settings
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models
from django.urls import reverse
from django.utils.timezone import now, timedelta
from django.utils.translation import gettext_lazy as _

from welco.qualif.models import Association


class PhoneCall(models.Model):
    class Meta:
        verbose_name = _('Phone Call')

    caller = models.CharField(_('Caller'), max_length=80)
    callee = models.CharField(_('Callee'), max_length=80)
    start = models.DateTimeField(_('Start'), auto_now_add=True)
    stop = models.DateTimeField(_('Stop'), null=True, blank=True)
    data = models.TextField(_('Data'), blank=True)

    # common to all source types:
    status = models.CharField(_('Status'), blank=True, max_length=50)
    contact_id = models.CharField(max_length=50, null=True)
    associations = GenericRelation(Association, content_type_field='source_type', object_id_field='source_pk')

    creation_timestamp = models.DateTimeField(auto_now_add=True)
    last_update_timestamp = models.DateTimeField(auto_now=True)

    @classmethod
    def get_qualification_form_class(cls):
        return None

    def get_qualification_form(self):
        return None

    @classmethod
    def get_qualification_form_submit_url(cls):
        return reverse('qualif-phone-save')

    @classmethod
    def get_current_calls(cls, user):
        if settings.PHONE_MAX_CALL_DURATION:
            logger = logging.getLogger(__name__)
            start_after = now() - timedelta(minutes=settings.PHONE_MAX_CALL_DURATION)
            for call in cls.objects.filter(
                callee__in=PhoneLine.get_callees(user), stop__isnull=True, start__lt=start_after
            ):
                logger.info('stop expired call from %s to %s', call.caller, call.callee)
                call.stop = now()
                call.save()
        return cls.objects.filter(callee__in=PhoneLine.get_callees(user), stop__isnull=True).order_by('start')

    @classmethod
    def get_all_callees(cls):
        return cls.objects.values_list('callee', flat=True).distinct()

    def get_source_context(self, request):
        return {
            'channel': 'phone',
        }

    def previous_calls(self):
        return PhoneCall.objects.filter(caller=self.caller).exclude(id=self.id).order_by('-start')[:5]

    @property
    def duration(self):
        if not self.stop:
            return 'n.a.'
        seconds = (self.stop - self.start).seconds
        return '%02d:%02d' % (seconds // 60, seconds % 60)


class PhoneLine(models.Model):
    callee = models.CharField(_('Callee'), unique=True, max_length=80)
    users = models.ManyToManyField('auth.User', verbose_name=_('User'))

    @classmethod
    def take(cls, callee, user):
        '''Take a line number'''
        line = cls.objects.get_or_create(callee=callee)[0]
        line.users.add(user)

    @classmethod
    def release(cls, callee, user):
        '''Release a line number'''
        line = cls.objects.get_or_create(callee=callee)[0]
        line.users.remove(user)

    @classmethod
    def get_callees(cls, user):
        '''Return all line numbers watched by user'''
        return PhoneLine.objects.filter(users=user).values_list('callee', flat=True)
