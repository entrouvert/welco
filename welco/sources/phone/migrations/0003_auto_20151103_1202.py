from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('phone', '0002_auto_20151028_1635'),
    ]

    operations = [
        migrations.AddField(
            model_name='phonecall',
            name='contact_id',
            field=models.CharField(max_length=50, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='phonecall',
            name='status',
            field=models.CharField(max_length=50, verbose_name='Status', blank=True),
            preserve_default=True,
        ),
    ]
