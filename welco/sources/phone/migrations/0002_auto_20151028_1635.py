from django.conf import settings
from django.db import migrations, models
from django.utils.timezone import now


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('phone', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='PhoneLine',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('callee', models.CharField(unique=True, max_length=20, verbose_name='Callee')),
                ('users', models.ManyToManyField(to=settings.AUTH_USER_MODEL, verbose_name='User')),
            ],
        ),
        migrations.RemoveField(
            model_name='phonecall',
            name='number',
        ),
        migrations.AddField(
            model_name='phonecall',
            name='callee',
            field=models.CharField(default='0', max_length=20, verbose_name='Callee'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='phonecall',
            name='caller',
            field=models.CharField(default='0', max_length=20, verbose_name='Caller'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='phonecall',
            name='data',
            field=models.TextField(verbose_name='Data', blank=True),
        ),
        migrations.AddField(
            model_name='phonecall',
            name='start',
            field=models.DateTimeField(default=now, verbose_name='Start', auto_now_add=True),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='phonecall',
            name='stop',
            field=models.DateTimeField(null=True, verbose_name='Stop', blank=True),
        ),
    ]
