from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('phone', '0003_auto_20151103_1202'),
    ]

    operations = [
        migrations.AlterField(
            model_name='phonecall',
            name='callee',
            field=models.CharField(max_length=80, verbose_name='Callee'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='phonecall',
            name='caller',
            field=models.CharField(max_length=80, verbose_name='Caller'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='phoneline',
            name='callee',
            field=models.CharField(unique=True, max_length=80, verbose_name='Callee'),
            preserve_default=True,
        ),
    ]
