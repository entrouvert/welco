# welco - multichannel request processing
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.urls import path, re_path

from . import views

urlpatterns = [
    path('ajax/phone/zone/', views.zone, name='phone-zone'),
    path('api/phone/call-event/', views.call_event, name='phone-call-event'),
    re_path(r'^api/phone/active-call/(?P<pk>\w+)/$', views.active_call, name='phone-active-call'),
    path('api/phone/current-calls/', views.current_calls, name='phone-current-calls'),
    path('api/phone/take-line/', views.take_line, name='phone-take-line'),
    path('api/phone/release-line/', views.release_line, name='phone-release-line'),
]
