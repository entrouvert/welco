# welco - multichannel request processing
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from ckeditor import views as ckeditor_views
from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.urls import include, path, re_path
from django.views.decorators.cache import never_cache

import welco.contacts.views
import welco.kb.views
import welco.views

from . import apps
from .kb.views import kb_manager_required

urlpatterns = [
    path('', welco.views.home, name='home'),
    path('mail/', welco.views.home_mail, name='home-mail'),
    path('phone/', welco.views.home_phone, name='home-phone'),
    path('counter/', welco.views.home_counter, name='home-counter'),
    path('', include('welco.sources.phone.urls')),
    path('', include('welco.sources.counter.urls')),
    path('ajax/qualification', welco.views.qualification, name='qualif-zone'),
    re_path(
        r'^ajax/remove-association/(?P<pk>\w+)$',
        welco.views.remove_association,
        name='ajax-remove-association',
    ),
    re_path(r'^ajax/create-formdata/(?P<pk>\w+)$', welco.views.create_formdata, name='ajax-create-formdata'),
    path('ajax/kb', welco.kb.views.zone, name='kb-zone'),
    path('kb/', welco.kb.views.page_list, name='kb-home'),
    path('kb/add/', welco.kb.views.page_add, name='kb-page-add'),
    path('kb/search/', welco.kb.views.page_search, name='kb-page-search'),
    path('kb/search/json/', welco.kb.views.page_search_json, name='kb-page-search-json'),
    re_path(r'^kb/(?P<slug>[\w-]+)/$', welco.kb.views.page_detail, name='kb-page-view'),
    re_path(r'^ajax/kb/(?P<slug>[\w-]+)/$', welco.kb.views.page_detail_fragment, name='kb-page-fragment'),
    re_path(r'^kb/(?P<slug>[\w-]+)/edit$', welco.kb.views.page_edit, name='kb-page-edit'),
    re_path(r'^kb/(?P<slug>[\w-]+)/delete$', welco.kb.views.page_delete, name='kb-page-delete'),
    path('ajax/contacts', welco.contacts.views.zone, name='contacts-zone'),
    path('contacts/search/json/', welco.contacts.views.search_json, name='contacts-search-json'),
    re_path(
        r'^ajax/contacts/(?P<slug>[\w-]+)/$',
        welco.contacts.views.contact_detail_fragment,
        name='contact-page-fragment',
    ),
    path('contacts/add/', welco.contacts.views.contact_add, name='contacts-add'),
    re_path(
        r'^ajax/summary/(?P<source_type>\w+)/(?P<source_pk>\w+)/$',
        welco.views.wcs_summary,
        name='wcs-summary',
    ),
    re_path(r'^admin/', admin.site.urls),
    path('logout/', welco.views.logout, name='auth_logout'),
    path('login/', welco.views.login, name='auth_login'),
    re_path(r'^menu.json$', welco.views.menu_json, name='menu_json'),
    re_path(r'^ckeditor/upload/', kb_manager_required(ckeditor_views.upload), name='ckeditor_upload'),
    re_path(
        r'^ckeditor/browse/', never_cache(kb_manager_required(ckeditor_views.browse)), name='ckeditor_browse'
    ),
]

if 'mellon' in settings.INSTALLED_APPS:
    urlpatterns.append(path('accounts/mellon/', include('mellon.urls')))

# static and media files
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns = apps.register_urls(urlpatterns)
