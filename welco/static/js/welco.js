$(function() {
  $.fn.extend({
    select_2levels: function () {
      this.each(function() {
        var $orig_select = $(this);
        $orig_select.hide();
        var $new_div = $('<ul class="result select-formdata"></ul>');
        $new_div.appendTo($orig_select.parent());
        $orig_select.find('optgroup').each(function(idx, optgroup) {
          var $new_li = $('<li class="cat"><span>' + $(optgroup).attr('label') + '</span><ul></ul></li>');
          var $new_sub_ul = $new_li.find('ul');
          $(optgroup).find('option').each(function(idx, option) {
            $('<li data-value="' + $(option).attr('value') + '">' + $(option).text() + '</li>').appendTo($new_sub_ul);
          });
          $new_li.appendTo($new_div);
        });
        $new_div.find('li.cat span').on('welco:click-on', function() {
            $(this).parents('ul').find('li').removeClass('on');
            $(this).parent().addClass('on');
            $new_div.addClass('expanded');
        });
        $new_div.find('li.cat span').on('click', function() {
          if ($(this).parent().hasClass('on')) {
            $(this).parent().removeClass('on');
            $new_div.removeClass('expanded');
          } else {
            $(this).parents('ul').find('li').removeClass('on');
            $(this).parent().addClass('on');
            $new_div.addClass('expanded');
            $('.cell.qualif').scrollTop(0);
          }
        });
        $new_div.find('[data-value]').on('click', function() {
          $orig_select.val($(this).data('value'));
          $orig_select.parents('div.add-formdef-reference').find('button.add').trigger('click');
        });
      });
    }
  });
  function refresh_bottom_cells(callback) {
    var source_type = $('div.source div[data-source-type]').data('source-type');
    var source_pk = $('div.source .active[data-source-pk]').data('source-pk');
    $('.cell[data-zone-url]').each(function(idx, zone) {
      $.ajax({url: $(zone).data('zone-url'),
          data: {source_type: source_type,
                 source_pk: source_pk},
          async: true,
          dataType: 'html',
          success: function(data) {
                  $(zone).find('> div').replaceWith(data);
                  $(zone).find('select').select_2levels();
                  $(zone).removeClass('has-page-displayed');
                  $(zone).removeClass('has-contact-displayed');
                  $('#create-new-contact').off('click').on('click', window.displayPopup);
                  if ($('#id_post_date').length && !$('#id_post_date').val()) {
                    $('div.qualif button.done').attr('disabled', 'disabled');
                  } else {
                    $('div.qualif button.done').removeAttr('disabled');
                  }
                  if (typeof(callback) === 'function') callback();
          },
          error: function(error) { console.log(':(', error); }
      });
    });
  }

  $('[data-pdf-href]').on('click', function() {
    var current_pdf = $('#pdf-viewer').data('current-pdf-href');
    var new_pdf = $(this).data('pdf-href');
    if (current_pdf !== new_pdf) {
      $('#pdf-viewer').data('current-pdf-href', new_pdf);
      $('#pdf-viewer')[0].contentWindow.postMessage(new_pdf,
                    window.location.protocol + '//' + window.location.host);
      $(this).trigger('welco:mail-selected');
    }
  });
  $('.mails ul li[data-pdf-href]').on('click', function() {
    $(this).parent().find('li').removeClass('active');
    $(this).addClass('active');
    $('#id_post_date').val($(this).data('post-date'));
    $('#id_registered_mail_number').val($(this).data('registered-mail-number'));
    $('#id_reference').val($(this).data('reference'));
    $('#id_subject').val($(this).data('subject'));
    var source_pk = $('div.source .active[data-source-pk]').data('source-pk');
    $('#postit > div.content').data('url', $('#postit > div.content').data('base-url') + '?mail=' + source_pk);
    $('#postit').trigger('welco:load-mail-note');
    $('#postit').show();
    refresh_bottom_cells(function() {
      var selected_category = $('#category-filter').val();
      if (selected_category) {
        $('.qualif ul.select-formdata span').each(function() {
          if ($(this).text() == selected_category) {
            $(this).trigger('welco:click-on');
          };
        });
      }
    });
  });

  $('.contacts').delegate('button.save', 'click', function() {
    var source_type = $('div.source div[data-source-type]').data('source-type');
    var source_pk = $('div.source .active[data-source-pk]').data('source-pk');
    var selected_user_id = $('#current-selected-user').val();
    $.ajax({url: $('.cell.contacts').data('zone-url'),
            data: {user_id: selected_user_id,
                   source_type: source_type,
                   source_pk: source_pk},
            method: 'POST',
            dataType: 'html',
            success: function(data) {
                    $('div.contacts h3').effect('highlight');
            },
            error: function(error) { console.log(':/', error); }
    });
    return false;
  });

  $('.qualif').delegate('a.remove', 'click', function() {
    var source_type = $('div.source div[data-source-type]').data('source-type');
    var source_pk = $('div.source .active[data-source-pk]').data('source-pk');
    $.ajax({url: $(this).attr('href'),
           method: 'GET',
           success: function(data) {
             $.ajax({url: $('.cell.qualif').data('zone-url'),
                    data: {source_type: source_type, source_pk: source_pk},
                    async: true,
                    dataType: 'html',
                    success: function(data) {
                      var zone = $('.cell.qualif');
                      $(zone).find('> div').replaceWith(data);
                      $(zone).find('select').select_2levels();
                      if ($('#id_post_date').length && !$('#id_post_date').val()) {
                        $('div.qualif button.done').attr('disabled', 'disabled');
                      } else {
                        $('div.qualif button.done').removeAttr('disabled');
                      }
                    }
             });
           }
    });
    return false;
  });


  $('.qualif').delegate('button.add', 'click', function() {
    var formdef_reference = $('#id_formdef_reference').val();
    var source_type = $('div.source div[data-source-type]').data('source-type');
    var source_pk = $('div.source .active[data-source-pk]').data('source-pk');
    $.ajax({url: $('.cell.qualif').data('zone-url'),
            data: {formdef_reference: formdef_reference,
                   source_type: source_type,
                   source_pk: source_pk},
            method: 'POST',
            dataType: 'html',
            success: function(data) {
                    $('.cell.qualif > div').replaceWith(data);
                    $('.cell.qualif').find('select').select_2levels();
                    if ($('#id_post_date').length && !$('#id_post_date').val()) {
                      $('div.qualif button.done').attr('disabled', 'disabled');
                    } else {
                      $('div.qualif button.done').removeAttr('disabled');
                    }
            },
            error: function(error) { console.log(':(', error); }
    });
    return false;
  });

  $('.qualif').delegate('button.done', 'click', function() {
    var formdef_reference = $('#id_formdef_reference').val();
    var source_type = $('div.source div[data-source-type]').data('source-type');
    var source_pk = $('div.source .active[data-source-pk]').data('source-pk');
    $.ajax({url: $(this).data('action-url'),
            data: {formdef_reference: formdef_reference,
                   source_type: source_type,
                   source_pk: source_pk},
            method: 'POST',
            dataType: 'json',
            success: function(data) {
              if (data.result == 'ok') {
                window.location.reload();
              } else {
                alert(data.msg);
              }
            },
            error: function(error) { console.log(':(', error); }
    });
    return false;
  });

  $('.document').delegate('button.reject', 'click', function() {
    if (confirm("Confirmer la suppression de ce courrier") != true) {
      return false;
    }
    var source_type = $('div.source div[data-source-type]').data('source-type');
    var source_pk = $('div.source .active[data-source-pk]').data('source-pk');
    $.ajax({url: $(this).data('action-url'),
            data: {source_type: source_type,
                   source_pk: source_pk},
            method: 'POST',
            dataType: 'html',
            success: function(data) {
                window.location.reload();
            },
            error: function(error) { console.log(':(', error); }
    });
    return false;
  });

  $('.document').delegate('input', 'change', function() {
    $('.document button.save').trigger('click');
  });

  $('.document').delegate('button.save', 'click', function() {
    var post_date = $('#id_post_date').val();
    var registered_mail_number = $('#id_registered_mail_number').val();
    var reference = $('#id_reference').val();
    var subject = $('#id_subject').val();
    var source_type = $('div.source div[data-source-type]').data('source-type');
    var source_pk = $('div.source .active[data-source-pk]').data('source-pk');
    $.ajax({url: $(this).data('action-url'),
            data: {post_date: post_date,
                   registered_mail_number: registered_mail_number,
                   reference: reference,
                   subject: subject,
                   source_type: source_type,
                   source_pk: source_pk},
            method: 'POST',
            dataType: 'html',
            success: function(data) {
                    $('div.source .active').data('post-date', post_date);
                    $('div.source .active').data('registered-mail-number', registered_mail_number);
                    $('div.source .active').data('reference', reference);
                    $('div.source .active').data('subject', subject);
                    $('#source-mainarea form').effect('highlight');
                    if ($('#id_post_date').length && !$('#id_post_date').val()) {
                      $('div.qualif button.done').attr('disabled', 'disabled');
                    } else {
                      $('div.qualif button.done').removeAttr('disabled');
                    }
            },
            error: function(error) { console.log(':(', error); }
    });
    return false;
  });

  $('.qualif').delegate('a.plus', 'click', function() {
    $(this).parents('li').hide();
    $('.qualif .add-formdef-reference').show();
  });

  $('.kb, .contacts').delegate('a.reset', 'click', function() {
    var source_type = $('div.source div[data-source-type]').data('source-type');
    var source_pk = $('div.source .active[data-source-pk]').data('source-pk');
    $(this).parents('.cell').each(function(idx, zone) {
      $.ajax({url: $(zone).data('zone-url'),
          data: {source_type: source_type,
                 source_pk: source_pk},
          async: true,
          dataType: 'html',
          success: function(data) {
                  var new_zone = $(data);

                  /* when going back to contact search make sure we are not
                   * jumped back to the selected contact */
                  new_zone.find('div.contact').empty();
                  $(zone).find('> div').replaceWith(new_zone);

                  if (new_zone.find('div.contact').length) {
                    /* unselect contact */
                    $.ajax({url: $('.cell.contacts').data('zone-url'),
                            data: {user_id: null,
                                   source_type: source_type,
                                   source_pk: source_pk},
                            method: 'POST',
                            dataType: 'html',
                            success: function(data) {
                            },
                            error: function(error) { console.log(':/', error); }
                    });
                  }

                  $(zone).find('select').select_2levels();
                  $(zone).removeClass('has-contact-displayed');
                  $(zone).removeClass('has-page-displayed');
          },
          error: function(error) { console.log(':(', error); }
      });
    });
  });

  $('.kb, .contacts').delegate('input', 'keyup', function() {
    var q = $(this).val();
    var search_result_ul = $(this).parent().find('ul.result');
    search_result_ul.empty();
    $.getJSON($(this).data('autocomplete-json'),
            {'q': q},
            function (response) {
                    search_result_ul.empty();
                    $(response.data).each(function(idx, elem) {
                       var new_elem = '<li><a href="#" data-page-slug="' + elem.slug + '">' + elem.title;
                       if (elem.roles) {
                         new_elem = new_elem + ' <span class="roles">' + elem.roles + '</span>';
                       }
                       if (elem.more) {
                         new_elem = new_elem + ' <span class="more">' + elem.more + '</span>';
                       }
                       new_elem = new_elem + '</a></li>'
                       $(new_elem).appendTo(search_result_ul);
                    });
            }
    );
    return false;
  });
  $('.kb').delegate('.tagcloud span', 'click', function() {
    $('.kb input').val($(this).text()).trigger('keyup');
  });
  $('.kb').delegate('ul.result a', 'click', function() {
    var page_slug = $(this).data('page-slug');
    var fragment_url = $(this).parents('[data-page-fragment-url]').data('page-fragment-url') + page_slug + '/';
    $.ajax({url: fragment_url,
            method: 'GET',
            dataType: 'html',
            success: function(data) {
              $('.kb .page').replaceWith(data);
              $('.kb').addClass('has-page-displayed');
            },
            error: function(error) { console.log(':(', error); }
    });

    return false;
  });
  $('.contacts').delegate('ul.result a', 'click', function() {
    var user_slug = $(this).data('page-slug');
    var fragment_url = $(this).parents('[data-contact-fragment-url]').data('contact-fragment-url') + user_slug + '/';
    var source_type = $('div.source div[data-source-type]').data('source-type');
    var source_pk = $('div.source .active[data-source-pk]').data('source-pk');
    $.ajax({url: fragment_url,
            method: 'GET',
            data: {source_type: source_type, source_pk: source_pk},
            dataType: 'html',
            success: function(data) {
              $('.contacts .contact').replaceWith(data);
              $('.contacts').addClass('has-contact-displayed');
              var selected_user_id = $('#current-selected-user').val();
              $.ajax({url: $('.cell.contacts').data('zone-url'),
                    data: {user_id: selected_user_id,
                           source_type: source_type,
                           source_pk: source_pk},
                    method: 'POST',
                    dataType: 'html',
                    success: function(data) {
                    },
                    error: function(error) { console.log(':/', error); }
              });
            },
            error: function(error) { console.log(':(', error); }
    });
    return false;
  });

  $('#pdf-viewer').on('load', function() {
    if (window.location.search) {
      $('.mails ul li[data-pdf-href][data-source-pk=' + window.location.search.substr(1) + ']').trigger('click');
    } else {
      $('.mails ul li[data-pdf-href]:first-child').trigger('click');
    }
  });

  $(document).on('gadjo:dialog-done welco:load-mail-note', function(ev) {
    if (ev.target && ev.target.id == 'create-new-contact') return;
    var source_pk = $('div.source .active[data-source-pk]').data('source-pk');
    $('#postit > div.content').load('/ajax/mail/note/' + source_pk);
  });

  $(document).on('gadjo:dialog-done', function(ev, data) {
    if (ev.target && ev.target.id != 'create-new-contact') return;
    if (data.err == 1) {
       alert('error: ' + data.data);
       return true;
    }
    var source_type = $('div.source div[data-source-type]').data('source-type');
    var source_pk = $('div.source .active[data-source-pk]').data('source-pk');
    var selected_user_id = data.data.user_id;
    $.ajax({url: $('.cell.contacts').data('zone-url'),
            data: {user_id: selected_user_id,
                   source_type: source_type,
                   source_pk: source_pk},
            method: 'POST',
            dataType: 'html',
            success: function(data) {
                    var fragment_url = $('.cell.contacts [data-contact-fragment-url]'
                                    ).data('contact-fragment-url') + selected_user_id + '/';
                    $.ajax({url: fragment_url,
                            method: 'GET',
                            data: {source_type: source_type, source_pk: source_pk},
                            dataType: 'html',
                            success: function(data) {
                              $('.contacts .contact').replaceWith(data);
                              $('.contacts').addClass('has-contact-displayed');
                            },
                            error: function(error) { console.log(':(', error); }
                    });
            },
            error: function(error) { console.log(':/', error); }
    });

  });

  $('#postit > div.content').on('click', function() {
    $('#postit').removeClass('closed');
  });
  $('#postit > div.content').on('click', window.displayPopup);
  $('#postit.readonly > div').off('click');
  $('#postit > div.hbar').on('click', function() {
    $('#postit').toggleClass('closed');
  });
  $('#id_post_date').datepicker($.datepicker.regional["fr"]);

  if ($('#category-filter').length) {
     var known_categories = Object();
     $('[data-category-name]').each(function(index, elem) {
         known_categories[$(elem).data('category-name')] = $(elem).data('category-name');
     });
     var options = Array();
     $(Object.keys(known_categories)).each(function(index, elem) {
         options.push({'key': elem, 'value': known_categories[elem]});
     });
     options.sort(function(x, y) {
         if (x.value > y.value) return 1;
         if (x.value < y.value) return -1;
         return 0;
     });
     $('#category-filter').empty();
     /* TODO: should use javascript i18n */
     $('<option value="">Toutes</option>').appendTo($('#category-filter'));
     $(options).each(function(index, elem) {
         $('<option value="' + elem.key + '">' + elem.value + '</option>').appendTo($('#category-filter'));
     });
     $('#category-filter').on('change', function() {
         var selected_category = $(this).val();
         window.sessionStorage.selected_category = selected_category;
         if (selected_category == '') {
             $('#source-sidebar li').removeClass('li-visible');
             $('#source-sidebar li').show();
         } else {
             $('#source-sidebar li').each(function(index, elem) {
                 var category_references = $(elem).find('[data-category-name]').map(function(idx, elem) {
                     return $(elem).data('category-name');
                 }).toArray();
                 if (category_references.indexOf(selected_category) == -1) {
                     $(elem).removeClass('li-visible');
                     $(elem).hide();
                 } else {
                     $(elem).addClass('li-visible');
                     $(elem).show();
                 }
             });
             if ($('#source-sidebar li.active.li-visible').length == 0) {
                 $($('#source-sidebar li.li-visible')[0]).trigger('click');
             }
         }
     });
     if (window.sessionStorage.getItem('selected_category') !== null) {
       var selected_category = window.sessionStorage.getItem('selected_category');
       $('#category-filter').val(selected_category).trigger('change');
     }
  }

  $('.document').delegate('button#take-phoneline', 'click', function() {
    var zone = $(this).parents('[data-zone-url]');
    var callee = $('input[name=phoneline]').val();
    if (callee == '') return false;
    $.ajax({url: $(this).data('action-url'),
          data: JSON.stringify({callee: callee}),
          method: 'POST',
          contentType: 'application/json',
          async: true,
          dataType: 'json',
          success: function(data) {
                  $.ajax({url: zone.data('zone-url'),
                          dataType: 'html',
                          success: function(data) {
                            $(zone).parent('div').html(data);
                          }
                  });
          },
          error: function(error) { console.log(':(', error); }
    });
    return false;
  });

  $('.document').delegate('a.release-line', 'click', function() {
    var zone = $(this).parents('[data-zone-url]');
    var callee = $(this).data('callee');
    $.ajax({url: $(this).data('action-url'),
          data: JSON.stringify({callee: callee}),
          method: 'POST',
          contentType: 'application/json',
          async: true,
          dataType: 'json',
          success: function(data) {
                  $.ajax({url: zone.data('zone-url'),
                          dataType: 'html',
                          success: function(data) {
                            $(zone).parent('div').html(data);
                          }
                  });
          },
          error: function(error) { console.log(':(', error); }
    });
    return false;

  });

  if ($('.source-phone').length) {
      var failed_ajax_calls = 0;

      function handle_ajax_error() {
        failed_ajax_calls += 1;
        if (failed_ajax_calls == 10) {
          window.location.reload();
        }
      }

      function check_active_call() {
        var active_call_url = $('[data-active-call-url]').data('active-call-url');
        $.ajax({url: active_call_url,
                dataType: 'json',
                success: function(response) {
                    failed_ajax_calls = 0;
                    if (response.active === false) {
                      $('<span class="done"> (terminé)</span>').appendTo($('div.phonecall.active h1'));
                      window.setTimeout(check_calls, 1000);
                    } else {
                      window.setTimeout(check_active_call, 5000);
                    }
                },
                error: function() {
                    handle_ajax_error();
                    window.setTimeout(check_active_call, 5000 + failed_ajax_calls * 1000);
                }
        });
      }

      function check_calls() {
          if ($('.phonecall').length && $('.phonecall .done').length == 0) { /* active phone call */
              window.setTimeout(check_active_call, 1000);
          } else {
              var current_calls_url = $('[data-current-calls]').data('current-calls');
              $.ajax({url: current_calls_url,
                      dataType: 'json',
                      success: function(response) {
                          failed_ajax_calls = 0;
                          if (response.data.calls.length != 0) {
                              /* got a call */
                              var caller = response.data.calls[0].caller;
                              var zone = $('.source-phone .source [data-zone-url]');
                              $.ajax({url: zone.data('zone-url'),
                                      dataType: 'html',
                                      success: function(data) {
                                            $(zone).parent('div').html(data);
                                            refresh_bottom_cells(function() {
                                                $('.contacts input[name=q]').val(caller).trigger('keyup');
                                            });
                                     }
                              });
                          }
                          window.setTimeout(check_calls, 1000);
                      },
                      error: function(error) {
                          handle_ajax_error();
                          window.setTimeout(check_calls, 1000 + failed_ajax_calls * 1000);
                      }
              });
          }
      }
      check_calls();
  }

  $('.qualif').delegate('a.create-formdata', 'click', function() {
    $(this).css('pointer-events', 'none');
    $.ajax({url: $(this).data('action-url'),
            data: {},
            method: 'POST',
            dataType: 'json',
            success: function(data) {
              if (data.result == 'ok') {
                window.open(data.url, '_wcs_tab');
                refresh_bottom_cells();
              } else {
                alert(data.msg);
              }
            },
            error: function(error) { console.log(':(', error); }
    });
    return false;
  });

  if ($('.counter.active').length) {
    refresh_bottom_cells();
  }

});
