# welco - multichannel request processing
# Copyright (C) 2015  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.urls import reverse
from django.utils.translation import gettext_lazy as _

from welco.utils import get_wcs_formdef_details, get_wcs_services, push_wcs_formdata


class Association(models.Model):
    source_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    source_pk = models.PositiveIntegerField()
    source = GenericForeignKey('source_type', 'source_pk')
    comments = models.TextField(blank=True, verbose_name=_('Comments'))
    formdef_reference = models.CharField(max_length=250, null=True)
    formdata_id = models.CharField(max_length=250, null=True)
    formdata_url_backoffice = models.URLField(null=True)

    def push(self, request):
        # push validated request to wcs
        context = {}
        if self.source.contact_id:
            context['user_id'] = self.source.contact_id
        context['summary_url'] = request.build_absolute_uri(
            reverse('wcs-summary', kwargs={'source_type': self.source_type_id, 'source_pk': self.source_pk})
        )
        context.update(self.source.get_source_context(request))
        self.formdata_id, self.formdata_url_backoffice = push_wcs_formdata(
            request, self.formdef_reference, context
        )
        self.save()

    @property
    def formdef_name(self):
        try:
            return get_wcs_formdef_details(self.formdef_reference).get('title')
        except AttributeError:
            return '?'

    @property
    def formdef_category(self):
        try:
            return get_wcs_formdef_details(self.formdef_reference).get('category')
        except AttributeError:
            return '?'

    @property
    def formdata_url(self):
        site, formdef = self.formdef_reference.split(':')
        wcs_site = get_wcs_services().get(site)
        return '%sbackoffice/management/%s/%s/' % (wcs_site.get('url'), formdef, self.formdata_id)
