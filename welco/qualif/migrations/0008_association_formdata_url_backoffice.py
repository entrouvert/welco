from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualif', '0007_remove_association_triaged'),
    ]

    operations = [
        migrations.AddField(
            model_name='association',
            name='formdata_url_backoffice',
            field=models.URLField(null=True),
        ),
    ]
