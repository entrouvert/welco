from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualif', '0003_association_comments'),
    ]

    operations = [
        migrations.AddField(
            model_name='association',
            name='user_id',
            field=models.CharField(max_length=50, null=True),
            preserve_default=True,
        ),
    ]
