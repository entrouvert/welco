from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualif', '0002_association_triaged'),
    ]

    operations = [
        migrations.AddField(
            model_name='association',
            name='comments',
            field=models.TextField(verbose_name='Comments', blank=True),
            preserve_default=True,
        ),
    ]
