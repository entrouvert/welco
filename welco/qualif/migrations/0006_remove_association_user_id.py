from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('qualif', '0005_auto_20151009_1110'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='association',
            name='user_id',
        ),
    ]
