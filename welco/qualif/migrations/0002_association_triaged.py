from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualif', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='association',
            name='triaged',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
