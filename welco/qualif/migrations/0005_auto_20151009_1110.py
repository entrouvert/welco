from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('qualif', '0004_association_user_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='association',
            name='formdatas',
        ),
        migrations.DeleteModel(
            name='FormdataReference',
        ),
        migrations.RemoveField(
            model_name='association',
            name='formdefs',
        ),
        migrations.DeleteModel(
            name='FormdefReference',
        ),
        migrations.AddField(
            model_name='association',
            name='formdata_id',
            field=models.CharField(max_length=250, null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='association',
            name='formdef_reference',
            field=models.CharField(max_length=250, null=True),
            preserve_default=True,
        ),
    ]
