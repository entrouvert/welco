from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('contenttypes', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Association',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('source_pk', models.PositiveIntegerField()),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FormdataReference',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('reference', models.CharField(max_length=250)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='FormdefReference',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('reference', models.CharField(max_length=250)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='association',
            name='formdatas',
            field=models.ManyToManyField(to='qualif.FormdataReference'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='association',
            name='formdefs',
            field=models.ManyToManyField(to='qualif.FormdefReference'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='association',
            name='source_type',
            field=models.ForeignKey(to='contenttypes.ContentType', on_delete=models.CASCADE),
            preserve_default=True,
        ),
    ]
