from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('qualif', '0006_remove_association_user_id'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='association',
            name='triaged',
        ),
    ]
