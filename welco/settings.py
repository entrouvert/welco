"""
Django settings for welco project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

import os

from django.conf import global_settings
from django.utils.translation import gettext_lazy as _

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '6_7axt+6zknzt_==uj#5lvy!59#+wpvts9qbwie2ci1@hz6&23'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'ckeditor',
    'haystack',
    'taggit',
    'welco.sources.counter',
    'welco.sources.mail',
    'welco.sources.phone',
    'welco.qualif',
    'welco.kb',
    'welco.contacts',
    'gadjo',
    'xstatic.pkg.select2',
    'rest_framework',
)

MIDDLEWARE = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'welco.urls'

WSGI_APPLICATION = 'welco.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'fr-fr'

TIME_ZONE = 'Europe/Paris'

USE_I18N = True


USE_TZ = True

LOCALE_PATHS = (os.path.join(BASE_DIR, 'welco', 'locale'),)

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_FINDERS = tuple(global_settings.STATICFILES_FINDERS) + ('gadjo.finders.XStaticFinder',)

STATICFILES_DIRS = (os.path.join(BASE_DIR, 'welco', 'static'),)

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'welco', 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

CKEDITOR_UPLOAD_PATH = 'uploads/'

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar_Own': [
            ['Source', 'Format', '-', 'Bold', 'Italic'],
            ['NumberedList', 'BulletedList'],
            ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
            ['Link', 'Unlink'],
            [
                'Image',
            ],
            [
                'RemoveFormat',
            ],
        ],
        'toolbar': 'Own',
    },
}

# Authentication settings
try:
    import mellon
except ImportError:
    mellon = None

if mellon is not None:
    AUTHENTICATION_BACKENDS = (
        'mellon.backends.SAMLBackend',
        'django.contrib.auth.backends.ModelBackend',
    )

LOGIN_URL = '/login/'
LOGIN_REDIRECT_URL = '/'
LOGOUT_URL = '/logout/'

MELLON_ATTRIBUTE_MAPPING = {
    'email': '{attributes[email][0]}',
    'first_name': '{attributes[first_name][0]}',
    'last_name': '{attributes[last_name][0]}',
}

MELLON_SUPERUSER_MAPPING = {
    'is_superuser': 'true',
}

MELLON_USERNAME_TEMPLATE = '{attributes[name_id_content]}'

MELLON_IDENTITY_PROVIDERS = []


# indexing
HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.whoosh_backend.WhooshEngine',
        'PATH': os.path.join(BASE_DIR, 'whoosh_index'),
    },
}

HAYSTACK_SIGNAL_PROCESSOR = 'haystack.signals.RealtimeSignalProcessor'

# mapping of channel to group/role *names*
CHANNEL_ROLES = {
    'mail': [],
    'phone': [],
    'counter': [],
}

# role allowed to manage knowledge base
KB_ROLE = None  # deprecated
KB_MANAGE_ROLES = []

# roles allowed to visit knowledge base
KB_ACCESS_ROLES = []

# bottom panels (kb, contacts, qualif)
SCREEN_PANELS = ['contacts', 'qualif']

# useful links for counter
COUNTER_LINKS = [{'label': 'Wikipedia', 'url': 'https://fr.wikipedia.org'}]

# phone system
PHONE_ONE_CALL_PER_CALLEE = True
PHONE_MAX_CALL_DURATION = 0  # in minutes, 0 stands for infinity
# If user is from SSO (ie django-mellon session), consider username
# as a phone line number and take it automatically.
PHONE_AUTOTAKE_MELLON_USERNAME = False

REST_FRAMEWORK = {}
REST_FRAMEWORK['DEFAULT_AUTHENTICATION_CLASSES'] = ['rest_framework.authentication.BasicAuthentication']

local_settings_file = os.environ.get(
    'WELCO_SETTINGS_FILE', os.path.join(os.path.dirname(__file__), 'local_settings.py')
)
if os.path.exists(local_settings_file):
    with open(local_settings_file) as fp:
        exec(fp.read())
