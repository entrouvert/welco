# welco - multichannel request processing
# Copyright (C) 2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json

import httmock
import requests
from django.contrib.contenttypes.models import ContentType
from django.core.files.base import ContentFile
from django.utils.encoding import force_str
from webtest import Upload

from welco.sources.mail.models import Mail


def test_viewer_view(app):
    resp = app.get('/mail/viewer/', status=302)
    assert resp.location == '?file='
    resp = resp.follow()
    assert resp.html.find('title').text == 'PDF.js viewer'

    resp = app.get('/mail/viewer/', {'file': 'tests/test.pdf'}, status=200)
    assert resp.html.find('title').text == 'PDF.js viewer'


def test_get_feeder_view(app, user):
    resp = app.get('/mail/feeder/', status=302)
    assert resp.location.startswith('/login/?next=')
    app.set_user(user.username)
    resp = app.get('/mail/feeder/', status=200)
    assert resp.html.find('h2').text == 'Mail Feeder'


def test_post_feeder_view(app, user):
    app.set_user(user.username)
    resp = app.post('/mail/feeder/', params={'mail': Upload('filename.txt', b'contents')}, status=302)
    assert resp.location == '/mail/feeder/'
    resp = resp.follow()
    assert resp.html.find('li', {'class': 'info'}).text == '1 files uploaded successfully.'


def test_qualification_save_view(settings, app, db):
    settings.KNOWN_SERVICES = {
        'wcs': {
            'demarches': {
                'url': 'http://wcs.example.net/',
            }
        }
    }
    mail = Mail.objects.create(content=ContentFile('foo', name='bar.txt'), subject='spam')
    assert not mail.contact_id
    source_type = ContentType.objects.get_for_model(Mail).pk
    resp = app.post(
        '/ajax/qualification-mail-save',
        params={'source_type': source_type, 'source_pk': mail.pk, 'subject': 'eggs'},
        status=302,
    )
    assert resp.location == '/ajax/qualification?source_type=%s&source_pk=%s' % (source_type, mail.pk)

    @httmock.urlmatch(netloc='wcs.example.net', path='/api/formdefs/', method='GET')
    def response_get(url, request):
        headers = {'content-type': 'application/json'}
        content = {
            'err': 0,
            'data': [
                {
                    'title': 'Foo',
                    'slug': 'foo',
                }
            ],
        }
        return httmock.response(200, content, headers)

    with httmock.HTTMock(response_get):
        resp = resp.follow()
    assert resp.html.find('option', {'value': 'demarches:foo'}).text == 'Foo'
    assert Mail.objects.get(id=mail.pk).subject == 'eggs'


def test_edit_note_view(app, user):
    resp = app.get('/ajax/mail/edit-note/', status=302)
    assert resp.location.startswith('/login/?next=')

    app.set_user(user.username)
    mail = Mail.objects.create(content=ContentFile('foo', name='bar.txt'), note='spam')
    resp = app.get('/ajax/mail/edit-note/', params={'mail': mail.pk}, status=200)
    assert resp.html.find('h2').text == 'Note'
    assert resp.html.find('textarea', {'name': 'note'}).text == 'spam'

    resp.form['note'] = 'eggs'
    resp = resp.form.submit()
    assert resp.content_type == 'text/html'
    assert resp.text == '{"result": "ok"}'
    assert Mail.objects.get(id=mail.pk).note == 'eggs'


def test_note_view(app, user):
    mail = Mail.objects.create(content=ContentFile('foo', name='bar.txt'))
    resp = app.get('/ajax/mail/note/%s' % mail.pk, status=302)
    assert resp.location.startswith('/login/?next=')

    app.set_user(user.username)
    resp = app.get('/ajax/mail/note/%s' % mail.pk, status=200)
    assert resp.text == '+'
    assert not Mail.objects.get(id=mail.pk).note  # mail object is unchanged


def test_reject_view(settings, app, user):
    settings.MAARCH_FEED = {
        'URL': 'http://maarch.example.net',
        'ENABLE': True,
        'USERNAME': 'xxx',
        'PASSWORD': 'yyy',
        'STATUS_REFUSED': 'FOO',
    }
    resp = app.post('/ajax/mail/reject', status=302)
    assert resp.location.startswith('/login/?next=')

    app.set_user(user.username)
    mail = Mail.objects.create(content=ContentFile('foo', name='bar.txt'), external_id='maarch-42')

    @httmock.urlmatch(netloc='maarch.example.net', path='/rest/res/resource/status', method='PUT')
    def response_ok(url, request):
        assert json.loads(force_str(request.body)) == {'status': 'FOO', 'resId': ['42']}
        headers = {'content-type': 'application/json'}
        content = {'maarch_say': 'ok'}
        return httmock.response(200, content, headers)

    with httmock.HTTMock(response_ok):
        resp = app.post('/ajax/mail/reject', params={'source_pk': mail.pk}, status=200)
    assert Mail.objects.count() == 0

    # errors
    mail = Mail.objects.create(content=ContentFile('foo', name='bar.txt'), external_id='maarch-42')

    @httmock.urlmatch(netloc='maarch.example.net', path='/rest/res/resource/status', method='PUT')
    def response_error1(url, request):
        raise requests.RequestException

    with httmock.HTTMock(response_error1):
        resp = app.post('/ajax/mail/reject', params={'source_pk': mail.pk})
    assert Mail.objects.get(id=mail.pk)

    @httmock.urlmatch(netloc='maarch.example.net', path='/rest/res/resource/status', method='PUT')
    def response_error2(url, request):
        return httmock.response(500)

    with httmock.HTTMock(response_error2):
        resp = app.post('/ajax/mail/reject', params={'source_pk': mail.pk})
    assert Mail.objects.get(id=mail.pk)

    @httmock.urlmatch(netloc='maarch.example.net', path='/rest/res/resource/status', method='PUT')
    def response_error3(url, request):
        return httmock.response(200, 'not a json')

    with httmock.HTTMock(response_error3):
        resp = app.post('/ajax/mail/reject', params={'source_pk': mail.pk})
    assert Mail.objects.get(id=mail.pk)


def test_mail_count_view(app, user):
    resp = app.get('/ajax/count/mail/', status=302)
    assert resp.location.startswith('/login/?next=')

    Mail.objects.create(content=ContentFile('foo', name='bar.txt'), status='done-42')
    Mail.objects.create(content=ContentFile('foo', name='bar.txt'), status='43')
    app.set_user(user.username)
    resp = app.get('/ajax/count/mail/', status=200)
    assert resp.content_type == 'application/json'
    assert resp.json == {'count': 1}
