# welco - multichannel request processing
# Copyright (C) 2020  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import pytest
from django.core.management import call_command
from django.core.management.base import CommandError

from welco.sources.mail.models import Mail


def test_feed_mail_command(settings, tmpdir, db):
    settings.MEDIA_ROOT = str(tmpdir)
    path1 = '%s/mail1.txt' % str(tmpdir)
    path2 = '%s/mail2.txt' % str(tmpdir)
    path3 = '%s/mail3.txt' % str(tmpdir)
    with open(path2, 'w') as fd:
        fd.write('not a PDF')
    with open(path3, 'w') as fd:
        fd.write('%PDF- is a PDF')

    call_command('feed_mail', '--category', 'foobar', path1, path2, path3)
    assert Mail.objects.count() == 1
    assert Mail.objects.all()[0].scanner_category == 'foobar'

    with pytest.raises(CommandError, match='nothing got imported'):
        call_command('feed_mail', '--category', path1, path2)
