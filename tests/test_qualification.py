# welco - multichannel request processing
# Copyright (C) 2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from unittest import mock

import pytest
from django.test import override_settings

from welco.forms import QualificationForm

pytestmark = pytest.mark.django_db

KNOWN_SERVICES = {'wcs': {'eservices': {'url': 'http://localhost/', 'title': 'Eservices', 'orig': 'welco'}}}


@mock.patch('welco.utils.requests.get')
def test_get_qualification(mocked_get, client):
    with override_settings(KNOWN_SERVICES=KNOWN_SERVICES):
        forms = mock.Mock()
        forms.json.return_value = {
            'data': [
                {
                    'category': 'Test',
                    'authentication_required': False,
                    'description': '',
                    'title': 'Test form',
                    'slug': 'test-form',
                }
            ],
            'err': 0,
        }
        mocked_get.return_value = forms

        user = mock.Mock()
        user.saml_identifiers = mock.Mock()
        user.saml_identifiers.exists.return_value = True
        user.saml_identifiers.first.return_value = mock.Mock(name_id='nameid')

        QualificationForm(user)
        assert 'backoffice-submission=on' in mocked_get.call_args[0][0]
        assert 'NameID=nameid' in mocked_get.call_args[0][0]
