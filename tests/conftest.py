# welco - multichannel request processing
# Copyright (C) 2018  Entr'ouvert
#
# This program is free software: you can redistribute it and/or modify it
# under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import django_webtest
import pytest


@pytest.fixture
def app(request):
    wtm = django_webtest.WebTestMixin()
    wtm._patch_settings()
    request.addfinalizer(wtm._unpatch_settings)
    return django_webtest.DjangoTestApp()


@pytest.fixture
def user(db):
    from django.contrib.auth.models import User

    user = User.objects.create(username='toto')
    user.set_password('toto')
    user.save()
    return user


@pytest.fixture
def mail_group(db, settings, user):
    from django.contrib.auth.models import Group

    # add mail group to default user
    group = Group.objects.create(name='mail')
    user.groups.add(group)

    # define authorization of mail group on mail channel
    channel_roles = getattr(settings, 'CHANNEL_ROLES', {})
    mail_roles = channel_roles.setdefault('mail', [])
    mail_roles.append('mail')
    return group


@pytest.fixture
def phone_group(db, settings, user):
    from django.contrib.auth.models import Group

    # add mail group to default user
    group = Group.objects.create(name='phone')
    user.groups.add(group)

    # define authorization of phone group on phone channel
    channel_roles = getattr(settings, 'CHANNEL_ROLES', {})
    phone_roles = channel_roles.setdefault('phone', [])
    phone_roles.append('phone')
    return group


@pytest.fixture
def counter_group(db, settings, user):
    from django.contrib.auth.models import Group

    # add mail group to default user
    group = Group.objects.create(name='counter')
    user.groups.add(group)

    # define authorization of counter group on counter channel
    channel_roles = getattr(settings, 'CHANNEL_ROLES', {})
    counter_roles = channel_roles.setdefault('counter', [])
    counter_roles.append('counter')
    return group


@pytest.fixture
def kb_group(db, settings, user):
    from django.contrib.auth.models import Group

    # add mail group to default user
    group = Group.objects.create(name='kb')
    user.groups.add(group)

    # define authorization of kb group to manage then knowledge base
    kb_manage_roles = getattr(settings, 'KB_MANAGE_ROLES', [])
    kb_manage_roles.append('kb')
    return group
